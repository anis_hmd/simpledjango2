FROM python:3.8
LABEL MAINTAINER="ANIS HAMIDI | anishamidii@yahoo.ir"

ENV PYTHONUNBUFFERED 1

RUN mkdir /simpledjango
WORKDIR /simpledjnago
COPY . /simpledjnago

RUN pip install --upgrade pip 
RUN pip install -r requirements.txt
RUN python manage.py migrate


CMD ["python","manage.py","runserver", "0.0.0.0:80"]

